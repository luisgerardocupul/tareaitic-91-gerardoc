'use strict';



var Tarea = require('../models/Tarea');
var express = require('express');
var router = express.Router();

/* GET tareas listing. */
router.get('/', async function (req, res) {
    const tareas = await Tarea.findAll();
    res.json(tareas);
});

router.post('/', async function (req, res) {

    let { Titulo, Detalle, Resumen, Cantidad } = req.body;

    let tarea = await Tarea.create({
        titulo: Titulo,
        detalle: Detalle,
        resumen: Resumen,
        cantidad: Cantidad

    });
    res.json(tarea);
});

router.delete('/:id', async function (req, res) {

    const id = req.params.id;

    await Tarea.destroy({
        where: { id: id }

    });

    res.json(true);

});

router.put('/:id', async function (req, res) {
    let { Titulo, Detalle, Resumen, Cantidad } = req.body;
    const id = req.params.id;
    await Tarea.update({


        titulo: Titulo,
        detalle: Detalle,
        resumen: Resumen,
        cantidad: Cantidad,

    }, {
        where: { id: id }
    }

    );

    res.json(true);

});

module.exports = router;
