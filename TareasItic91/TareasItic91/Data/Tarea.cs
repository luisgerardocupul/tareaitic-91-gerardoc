﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TareasItic91.Data
{
    public class Tarea
    {
        public long id { get; set; }
        public string Titulo { get; set; }
        public string Detalle { get; set; }
        public string Resumen { get; set; }
        public string Cantidad { get; set; }
    }
}
