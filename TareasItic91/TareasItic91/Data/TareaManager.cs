﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using static Android.Renderscripts.ProgramVertexFixedFunction;

namespace TareasItic91.Data
{
    public class TareaManager
    { 
        const string url = "http://192.168.1.71:3000/tareas";
        public async Task<IEnumerable<Tarea>> GetAll()
        {
            HttpClient client = new HttpClient();
            string result = await client.GetStringAsync(url);
            return JsonConvert.DeserializeObject<IEnumerable<Tarea>>(result);
        }
            
        public async Task<Tarea> AddTarea(string titulo, string detalle, string resumen, string cantidad)
        {
            Tarea tarea = new Tarea()
            {
                Titulo = titulo,
                Detalle = detalle,
                Resumen = resumen,
                Cantidad = cantidad

            };

            HttpClient client = new HttpClient();
            var response = await client.PostAsync(url, new StringContent(
                JsonConvert.SerializeObject(tarea), 
                Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<Tarea>(    
                await response.Content.ReadAsStringAsync());


        }
     
    }
}
